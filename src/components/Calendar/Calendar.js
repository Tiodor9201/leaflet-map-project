export default {
    name: 'contracam-header',
    data () {
        return {
            month: new Date().getMonth(),
            year: new Date().getFullYear(),
            dFirstMonth: '1',
            day:["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
            months:["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            date: new Date(),
        }
    },
    computed: {
        dayChange: function(){
            if(this.dFirstMonth == 0){
                this.day = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"]
            } else {
                this.day = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
            }
        },
    },
    methods: {
        methods:{
            calendar: function() {
                let days = [];
                let week = 0;
                days[week] = [];
                let dlast = new Date(this.year, this.month + 1, 0).getDate();
                for (let i = 1; i <= dlast; i++) {
                    if (new Date(this.year, this.month, i).getDay() != this.dFirstMonth) {
                        let a = {index:i};
                        days[week].push(a);
                    } else {
                        week++;
                        days[week] = [];
                        let a = {index:i};
                        days[week].push(a);
                    }
                }
                if (days[0].length > 0) {
                    for (let i = days[0].length; i < 7; i++) {
                        days[0].unshift('');
                    }
                }
                this.dayChange();
                return days;
            },
            decrease: function() {
                this.month--;
                if (this.month < 0) {
                    this.month = 12;
                    this.month--;
                    this.year--;
                }
            },
            increase: function() {
                this.month++;
                if (this.month > 11) {
                    this.month = -1;
                    this.month++;
                    this.year++;
                }
            },
        }
    }
}
