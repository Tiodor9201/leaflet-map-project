/* eslint-disable no-undef */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import Vue2Filters from 'vue2-filters'
import VeeValidate from 'vee-validate'
import VueEventBus from 'vue-event-bus'
import VueAxios from 'vue-axios'
import axios from 'axios'
import VueStomp from 'vue-stomp'
import Leaflet from 'leaflet'

import App from './App'
import router from './router'

import VueDragDrop from 'vue-drag-drop'

import 'element-ui/lib/theme-chalk/index.css'
import './scss/element-variables.scss'

import VueCookie from 'vue-cookie'

Vue.use(VueCookie)

Vue.use(VueEventBus)
Vue.use(VeeValidate, { fieldsBagName: 'veeFields' })
Vue.use(Vue2Filters)
Vue.use(ElementUI, { locale })
Vue.use(Leaflet)

Vue.config.productionTip = false

axios.defaults.baseURL = '/englishlab/'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.common['Content-Type'] = 'application/json'
Vue.use(VueAxios, axios)

Vue.use(VueDragDrop)


router.afterEach((to, from) => {

})

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App },
    render: h => h(App)
})

Vue.use(VueStomp)
