import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../pages/MainLayout.vue'
import Map from '../pages/Map/Map.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: __dirname,
    routes: [
        { path: '/',
            component: Layout,
            children: [
                {name: 'Map', path: '/map', component: Map}
            ]
        }
    ]
})
